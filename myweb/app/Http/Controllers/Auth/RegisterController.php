<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
// use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\OtpCode;
use App\Http\Requests\Auth\RegisterRequest;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        request()->validate([
            
        ]);

        $role_user = Role::where('role_name','=','User')->first();
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'role_id' => $role_user->id,
            'password' => '8324hrfksdfhLKASEJFOIEWJ94jf29LKJASD99sjE9j4jD0sdv9wjw3fj',
        ]);

        $otp = OtpCode::updateOrCreate(
            ['user_id'   => $user->id],
            [
                'otp_code' => rand(100000,999999),
                'valid_until' => date("Y-m-d H:i:s",strtotime("+5 min")),
                'user_id'   => $user->id
        ]);

        event(new UserRegisteredEvent($user,$otp));

        return response()->json([
            "response_code" => "00",
            "reponse_message" => "Akun berhasil didaftarkan. Silahkan cek email",
            "data" => [compact('user'), 'otp_code' => $otp->otp_code],
        ]);
    }
}
