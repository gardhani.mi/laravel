<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
        ]);

        $user = User::where('email','=',$request['email'])->first();
        if($user->email_verified_at == null){
            return response()->json([
                "response_code" => "00",
                "reponse_message" => "Email belum terverifikasi",
            ]);
        }

        if($user->password == '8324hrfksdfhLKASEJFOIEWJ94jf29LKJASD99sjE9j4jD0sdv9wjw3fj'){
            return response()->json([
                "response_code" => "00",
                "reponse_message" => "Password akun belum didaftarkan. Silahkan lakukan update password.",
            ]);
        }

        if(!$token = auth()->attempt($request->only('email','password'))){
            return response()->json([
                "response_code" => "01",
                "reponse_message" => "Email dan Password tidak sesuai",
            ]);
        }

        $user = User::where('email','=',request('email'))->first();

        return response()->json([
            "response_code" => "00",
            "reponse_message" => "User berhasil login",
            "data" => [
                'token' => $token,
                'user' => $user
            ],
        ]);
    }
}
